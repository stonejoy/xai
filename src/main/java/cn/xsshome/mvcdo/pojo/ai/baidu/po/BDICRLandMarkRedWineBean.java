package cn.xsshome.mvcdo.pojo.ai.baidu.po;
/**
 * 接口返回的对象 针对于landmark,redwine通用
 * @author 小帅丶
 * @date 2019年3月19日
 * <p>Description: landmark,redwine融合对象</p>
 */
public class BDICRLandMarkRedWineBean {
	private String error_msg;
	private String error_code;
	private long log_id;
	private Result result;
	
	public long getLog_id() {
		return log_id;
	}

	public void setLog_id(long log_id) {
		this.log_id = log_id;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
	public String getError_msg() {
		return error_msg;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public static class Result{
		private String landmark;
		private String redwine;
		public String getLandmark() {
			return landmark;
		}
		public void setLandmark(String landmark) {
			this.landmark = landmark;
		}
		public String getRedwine() {
			return redwine;
		}
		public void setRedwine(String redwine) {
			this.redwine = redwine;
		}
	}
}
